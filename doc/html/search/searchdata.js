var indexSectionsWithContent =
{
  0: "_acdemorsw~",
  1: "cew",
  2: "m",
  3: "w",
  4: "adrsw~",
  5: "_",
  6: "co",
  7: "e",
  8: "o"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues"
};

var indexSectionLabels =
{
  0: "Указатель",
  1: "Классы",
  2: "Пространства имен",
  3: "Файлы",
  4: "Функции",
  5: "Переменные",
  6: "Определения типов",
  7: "Перечисления",
  8: "Элементы перечислений"
};

