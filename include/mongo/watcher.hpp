#ifndef __INCLUDE_MONGO_WATCHER_HPP__
#define __INCLUDE_MONGO_WATCHER_HPP__

#pragma once

#include <condition_variable>
#include <cstdint>
#include <functional>
#include <mutex>
#include <string>
#include <unordered_map>

#include <bsoncxx/document/view.hpp>

namespace mongo
{
    ///
    /// \brief Наблюдение за таблицей oplog и генерация событий
    ///
    /// Подключение к локальному серверу MongoDB на стандартном порту:
    /// \code
    ///    mongo::watcher w;
    ///    const int evtid = w.attach(mongo::watcher::on_modify, "test.table",
    ///        [](const mongo::watcher::evtinfo_t& info)
    ///        {
    ///        }
    ///    );
    ///    w.detach(evtid);
    /// \endcode
    ///
    /// Подключение к заданному серверу MongoDB:
    /// \code
    ///    mongo::watcher::config cfg;
    ///    cfg._uri = "...";
    ///    mongo::watcher w(cfg);
    ///    const int evtid = w.attach(mongo::watcher::on_modify, "test.table",
    ///        [](const mongo::watcher::evtinfo_t& info)
    ///        {
    ///        }
    ///    );
    ///    w.detach(evtid);
    /// \endcode
    ///
    class watcher
    {
    public:

        /// Конфигурация наблюдателя
        struct config_t
        {
            std::string _uri;     ///< mongodb uri для подключения
            std::string _db;      ///< имя базы в которой содержится таблица oplog.rs
            unsigned    _period;  ///< промежуток между проверками в миллисекундах
        };

        /// Идентификаторы событий, используется в функциях watcher::attach, watcher::detach
        enum event_t : int
        {
            on_insert = 1,                                  ///< маска события вставки документа
            on_update = 2,                                  ///< маска события модификации документа
            on_remove = 4,                                  ///< маска события удаления документа
            on_modify = on_insert | on_update | on_remove,  ///< маска события вставки/модификации/удаления документа
            on_noop = 8,                                    ///< маска "пустого" события
            on_any = on_modify | on_noop                    ///< маска для любого события
        };

        /// Данные события
        struct evtinfo_t
        {
            using obj_t = bsoncxx::document::view;

            int         _evtid; ///< идентфикатор обработчика события в классе watcher
            event_t     _evt;   ///< идентификатор типа события
            uint32_t    _ts;    ///< метка времени Unix
            std::string _ns;    ///< БД и коллекция для которой произошло события в формате "db.name"
            obj_t       _o;     ///< данные объекта, связанного с событием
            obj_t       _o2;    ///< только для on_update: критерий по которому обновлялся документ
        };

        /// Тип для функции обратного вызова
        using callback_t = std::function<void(const evtinfo_t&)>;

    public:
        /// Конструктор по умолчанию, подключается к локальному серверу MongoDB на стандартному порту
        watcher() noexcept;

        /// Конструктор, для подключения к заданному серверу MongoDB
        watcher(config_t cfg);

        /// Деструктор
        ~watcher();

        /// Подписка обработчика на события
        ///
        /// \param operations - маска операций, см. mongo::watcher::event_t
        /// \param collection - имя базы и коллекции, например: "db.collection_name"
        /// \param callback - обработчик события
        ///
        /// \return int - уникальный идентификатор подписки, используется в последующей отписке от событий
        ///
        /// * подписка на любое событие от любой коллекции:
        /// \code
        ///     int evtid = w.attach(on_modify, "", [](const evtinfo_t& ei) {...});
        /// \endcode
        ///
        /// * подписка на любое событие от коллекции name базы данных testdb:
        /// \code
        ///     int evtid = w.attach(on_modify, "testdb.name", [](const evtinfo_t& ei) {...});
        /// \endcode
        ///
        int attach(int operations, const std::string& collection, callback_t callback);

        /// Отписка обработчика от событий (возможно частичная)
        ///
        /// \param evtid - уникальный идентификатор подписки, возвращенный watcher::attach
        /// \param operations - маска операций, см. mongo::watcher::event_t
        /// \param collection - имя базы и коллекции, например: "db.collection_name"
        ///
        /// \return Количество событий от которых отписались
        ///
        /// * Отписка обработчика от событий вставки любой коллекции
        /// \code
        ///     w.detach(evtid, on_insert, "")
        /// \endcode
        ///
        /// * Отписка обработчика от всех событий коллекции name базы данных testdb:
        /// \code
        ///     w.detach(evtid, on_any, "testdb.name")
        /// \endcode
        ///
        int detach(int evtid, int operations = on_any, const std::string& collection = "");

        ///
        /// Запускает мониторинг событий MongoDB
        ///
        /// \param all - false : только события начиная с текущего времени
        ///              true : все события, в том числе и из прошлого
        ///
        void run(bool all = false);

        ///
        /// Останавливает мониторинг событий MongoDB
        ///
        void stop();

    private:
        using filter_t = std::pair<std::string, std::string>;

        struct hash {
            size_t operator()(const filter_t& f) const {
                return std::hash<std::string>()(f.first) ^ std::hash<std::string>()(f.second);
            }
        };

        using callbacks_t = std::unordered_map<filter_t, std::unordered_map<int, callback_t>, hash>;
        using cv_t = std::condition_variable;
        using rmutex_t = std::recursive_mutex;
        using mutex_t = std::mutex;

        config_t            _cfg = {"", "local", 1000};
        int                 _evtid_gen = 0;
        bool                _running = false;
        mutex_t             _mtx_stop;
        rmutex_t            _mtx_modify;
        cv_t                _cv;
        callbacks_t         _callbacks;

        void attach(int evtid, callback_t cb, const std::string& op, const std::string& c);
        int detach(int evtid, const std::string& op, const std::string& c);
        void _run(bool all);
        void run_callbacks(const filter_t& f, evtinfo_t& info);
    };

}

#endif // __INCLUDE_MONGO_WATCHER_HPP__
