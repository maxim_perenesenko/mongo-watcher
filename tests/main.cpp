#include "pre.hpp"

#define DOCTEST_CONFIG_IMPLEMENT
#include "doctest.h"

#include <mongocxx/instance.hpp>
#include <mongocxx/logger.hpp>


struct logger : public mongocxx::logger
{
    virtual void operator()(mongocxx::log_level level,
        bsoncxx::stdx::string_view domain,
        bsoncxx::stdx::string_view message) noexcept override
    {
        std::cout << mongocxx::to_string(level) << ", " << domain << ", " << message << std::endl;
    }
};

int main(int argc, char *argv[])
{
    doctest::Context context;

    mongocxx::instance inst{std::make_unique<logger>()};

    // defaults
    context.setOption("abort-after", 5);              // stop test execution after 5 failed assertions
    context.setOption("order-by", "name");            // sort the test cases by their name

    context.applyCommandLine(argc, argv);

    // overrides
    context.setOption("no-breaks", true);             // don't break in the debugger when assertions fail

    int res = context.run(); // run

    if (std::getenv("TESTPAUSE"))
    {
        std::cout << "Finished, press [enter]...";
        std::cin.get();
    }

    if(context.shouldExit()) // important - query flags (and --exit) rely on the user doing this
        return res;          // propagate the result of the tests

    int client_stuff_return_code = 0;
    // your program - if the testing framework is integrated in your production code

    return res + client_stuff_return_code; // the result from doctest is propagated here as well
}
